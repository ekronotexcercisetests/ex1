#include <iostream>

int test1()
{
	std::cout << "Test 1 executed" << std::endl;
	return 0;
}


int test2()
{
	std::cout << "Test 2 executed" << std::endl;
	return 0;
}

int test3()
{
	std::cout << "Test 3 executed" << std::endl;
	return 0;
}

int main(int argc, char* argv[])
{
	if (argc < 2)
	{
		std::cout << "Not enough args" << std::endl;
	}
	else
	{
		int option = atoi(argv[1]);
		switch (option)
		{
		case 1:
			return test1();
		case 2:
			return test2();
		case 3:
			return test3();

		}
	}
	return 0;
}